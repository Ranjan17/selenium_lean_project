package com.ps.driver;

import com.ps.constants.FrameworkConstants;
import com.ps.utils.ReadPropertyFile;
import org.openqa.selenium.chrome.ChromeDriver;
import java.util.Objects;

public final class Driver {
    private Driver(){}

    public static void initDriver() throws Exception {
        if (Objects.isNull(DriverManager.getDriver())){
            System.setProperty("webdriver.chrome.driver", FrameworkConstants.getChromedriverPath());
            DriverManager.setDriver(new ChromeDriver());
            DriverManager.getDriver().get(ReadPropertyFile.getValue("url"));
        }

    }

    public static void tearDriver(){
        if (Objects.nonNull(DriverManager.getDriver())){
            DriverManager.getDriver().quit();
            DriverManager.unload();
        }

    }
}
