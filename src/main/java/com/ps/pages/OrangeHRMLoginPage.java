package com.ps.pages;

import com.ps.driver.DriverManager;
import org.openqa.selenium.By;

public final class OrangeHRMLoginPage extends BasePage{

    private   final By userName_txtBox = By.name("username");
    private   final By password_txtBox = By.name("password");
    private   final By login_btn = By.xpath("button[class*=\"orangehrm-login-button\"]");

    public OrangeHRMLoginPage enterUserName(String userName){
        sendKeys(userName_txtBox,userName);
        return this;
    }

    public OrangeHRMLoginPage enterPassword(String password){
        sendKeys(password_txtBox,password);
        return this;
    }

    public OrangeHRMHomePage clickLoginBtn(){
        click(login_btn);
        return new OrangeHRMHomePage();
    }


    public String getTitle() {
        return getPageTitle();
    }
}
