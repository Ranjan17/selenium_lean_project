package com.ps.constants;

public final class FrameworkConstants {

    private FrameworkConstants(){}

    private static final String TESTRESOURCEPATH=System.getProperty("user.dir")+"/src/test/resources";
    private static final String CHROMEDRIVERPATH=TESTRESOURCEPATH+ "/drivers/chromedriver.exe";
    private static final String CONFIGFILEPATH=TESTRESOURCEPATH+ "/config/config.properties";

    public static String getChromedriverPath() {
        return CHROMEDRIVERPATH;
    }

    public static String getConfigfilepath(){return CONFIGFILEPATH; }
}
