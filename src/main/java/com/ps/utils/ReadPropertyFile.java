package com.ps.utils;

import com.ps.constants.FrameworkConstants;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;

public final class ReadPropertyFile {

    private ReadPropertyFile(){}

    private static final Properties property= new Properties();
    private static final Map<String, String> CONFIGMAP = new HashMap<>();
    static {
        try {
            FileInputStream file = new FileInputStream(FrameworkConstants.getConfigfilepath());
            property.load(file);
            /*for (Map.Entry<Object, Object> entry : property.entrySet()){
                CONFIGMAP.put(String.valueOf(entry.getKey()), String.valueOf(entry.getValue()));
            }*/

            property.forEach((key, value) -> CONFIGMAP.put(String.valueOf(key), String.valueOf(value)).trim());
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    public static String get(String key) throws Exception {
        if (Objects.isNull(key) || Objects.isNull(CONFIGMAP.get(key))){
            throw  new Exception("Property Name "+key+ " is not found. Please check the config.properties file");
        }
        return CONFIGMAP.get(key).trim();
    }
    public static String getValue(String key) throws Exception {
        if (Objects.isNull(property.getProperty(key)) || Objects.isNull(key)){
            throw  new Exception("Property Name "+key+ " is not found. Please check the config.properties file");
        }
        return property.getProperty(key).trim();

    }
}
