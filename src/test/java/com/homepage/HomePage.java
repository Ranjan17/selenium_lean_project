package com.homepage;

import com.ps.driver.DriverManager;
import org.assertj.core.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public final class HomePage extends BaseTest{
    private HomePage(){}

    @Test
    public void homePageTest() throws InterruptedException {
        DriverManager.getDriver().findElement(By.name("q")).sendKeys("appli tool", Keys.ENTER);
        Thread.sleep(3000);
        String title = DriverManager.getDriver().getTitle();
        /*Assert.assertTrue(Objects.nonNull(title), "Title is null");
        Assert.assertTrue(title.toLowerCase().contains("google search"));
        Assert.assertTrue(title.toLowerCase().matches("\\w.*"+"google search"));
        Assert.assertTrue(title.length()>8);*/

        //Use Assertj API
        Assertions.assertThat(title)
                .isNotNull()
                .containsIgnoringCase("google search")
                .matches("\\w.*"+"google search")
                .hasSize(10);

        List<WebElement> links = DriverManager.getDriver().findElements(By.xpath("//h3/span"));
        //Assert.assertTrue(links.size()>10);

        boolean isElementPresent= false;
        for (WebElement ele : links){
            if (ele.getText().contains("Applitools: Visually Perfect Applications with Automated Visual ...")){
                isElementPresent= true;
                break;
            }
        }
      //  Assert.assertTrue(isElementPresent, "Appli tools not found");

    }
    @Test
    public void homePageTest2(){
        DriverManager.getDriver().findElement(By.name("q")).sendKeys("Amazon", Keys.ENTER);
    }

}
