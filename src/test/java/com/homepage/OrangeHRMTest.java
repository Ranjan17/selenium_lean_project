package com.homepage;

import com.ps.pages.OrangeHRMHomePage;
import com.ps.pages.OrangeHRMLoginPage;
import org.assertj.core.api.Assertions;
import org.testng.annotations.Test;

public final class OrangeHRMTest extends BaseTest{

    private OrangeHRMTest(){}

    @Test
    public void loginTest(){
        String title = new OrangeHRMLoginPage()
                .enterUserName("Admin").enterPassword("admin123").clickLoginBtn()
                .clickWelcome().clickLogout()
                .getTitle();

        Assertions.assertThat(title)
                .isEqualTo("OrangeHRM");
    }


}
